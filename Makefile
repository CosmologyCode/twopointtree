
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# Makefile for TwoPointCorr.MPI 
# 
# Written by:
# Robert E. Smith, 
# Institute for Theoretical Physics,
# University of Zurich (2007)
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# REFERENCES:
# 
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# The routine for generation of the KD-tree was based upon:
#
# Michael Buhl and Matthew Kennel,
# Institute for Nonlinear Science (2002).
# "KDTREE 2: Fortran 95 and C++ software to efficiently search 
# for near neighbors in a multi-dimensional Euclidean space"
# http://arxiv.org/abs/physics/0408067
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# The two-point correlation function routines were based upon:
#
# "Fast Algorithms and Efficient Statistics: N-point correlation functions"
# Andrew Moore (CMU), Andy Connolly (UPitt), Chris Genovese (CMU), Alex Gray (CMU),
# Larry Grone (CMU), Nick Kanidoris II (CMU), Robert Nichol (CMU), Jeff Schneider (CMU), 
# Alex Szalay (JHU), Istvan Szapudi (CITA), Larry Wasserman (CMU),
# in Proceedings of MPA/MPE/ESO Conference "Mining the Sky", 2000, Garching, Germany 
# astro-ph/0012333  
#
# "The skewness of the aperture mass statistic"
# Michael Jarvis, Gary Bernstein, Bhuvnesh Jain, 2004, 
# Monthly Notices of the Royal Astronomical Society, 352, 338.
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#
# The random number generation is performed using the:
#
# Subtract-and-borrow random number generator proposed by
# Marsaglia and Zaman, implemented by F. James with the
# name RCARRY in 1991, and later improved by Martin Luescher          
# in 1993 to produce "Luxury Pseudorandom Numbers". 
#
# Fortran 77 coded by F. James, 1993
# references: 
# M. Luscher, Computer Physics Communications  79 (1994) 100
# F. James, Computer Physics Communications 79 (1994) 111
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# NOTE: THIS IS AN MPI CODE AND MUST BE COMPILED 
# WITH A SUITABLE MPI COMPILER:  

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# CHOOSE YOUR SYSTEM

SYSTYPE="MAC"
#SYSTYPE="ZBOX2"

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# HOME DIRECTORY

home = ${HOME}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# CODE DIRECTORIES

WD = ${shell pwd}

SOURCE1   = TwoPointCorr.MPI

AUX_DIR   = ${WD}/auxilliary/

RANDOM_DIR   = ${WD}/RANDOM/

OBJ_DIR   = ${WD}/objects/

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# <<< MAC >>>

ifeq ($(SYSTYPE),"MAC")

mpilib = ${home}/WORK/lib64/OPEN-MPI/openmpi-4.0.5-install/

# compiler

MPIF90   =   mpif90 

F90FLAGS    =   -O3 

# modules

MOD0 = -I${mpilib}/include

MOD1 = -I${WD}/modules

MODS = ${MOD0} ${MOD1} 

# libraries

LIB0 = -L${mpilib}/lib -lmpi

LIB1 = -L${WD}/libs -lTwoPointMPI

LIB2 = -L${WD}/RANDOM -lRANLUX

LIBS = ${LIB0} ${LIB1} ${LIB2}

endif

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# <<< ZBOX2 >>>

ifeq ($(SYSTYPE),"ZBOX2")

whichlib = 

# compiler

MPIF90   =   mpif90 

F90FLAGS    =   -O3 -mcmodel=medium

# modules

MOD0 = 

MOD1 = -I${WD}/modules

MODS = ${MOD0} ${MOD1} 

# libraries

LIB0 = -lmpich

LIB1 = -L${WD}/libs -lTwoPointMPI

LIB2 = -L${WD}/RANDOM -lRANLUX

LIBS = ${LIB0} ${LIB1} ${LIB2}

endif

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# FIRST make all of the auxilliary files

makeall: mods random arch maketwopt makeexe 

mods:   
	cd ${AUX_DIR} && make 


random: 
	cd ${RANDOM_DIR} && make 

arch:
	cd ${OBJ_DIR} && make 


maketwopt: ${SOURCE1}.f90
	${MPIF90} ${MODS} -c ${F90FLAGS} ${SOURCE1}.f90


makeexe: ${SOURCE1}.o 
	${MPIF90} ${SOURCE1}.o -o ${SOURCE1}.exe ${LIBS}
	\rm *.o
