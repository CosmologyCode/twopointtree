#!/bin/csh

echo Generating kD-tree 2-pt correlation functions

set rMIN = 1.0     # min radius to correlate

set rMAX = 140.0    # max radius to correlate

set nbins = 70     # number of bins

set nHaloBins = 6  # number of halo bins

set iBin = 1       # binning : iBin=0 => linear ; iBin=1 => logarithmic

set numprocs = 4

# number of particles to correlate

set nPartToCorr = ( 10 50 100 500 1000 5000 10000 50000 100000 500000 1000000)

set inPart = 11

while ( $inPart <= 11) 

set iRED = 3
set NRED = 3

while ( $iRED <= $NRED ) 

set iENSEMB = 1
set NENSEMB = 8

while ( $iENSEMB <= $NENSEMB )

set iSNAP = 16
set NSNAP = 16

while ( $iSNAP <= $NSNAP )

# snapshot

#-----------
# 750^3 DATA
set PATH = /smaug/data/theorie/res/ZBOX2
set SNAPBASE = LCDM-L1500-N750-Tf_om_m_0.25_om_de_0.75_om_b_0.04_sig8_0.8_h_0.7_
set SNAPEXT = ( 001 002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 018 019 020 021 022 023)
set SNAPNFILES = 10
set SNAPDATA = ${PATH}/LCDM-750-run${iENSEMB}/DATA/

#-----------
# 400^3 DATA
#set PATH = /smaug/data/theorie/res/LCDM-opteron-PENN/LCDM-Tf_0p27_0p73_0p72_z0.0
#set SNAPBASE = LCDM-Lb512-Np400-Tf_0p27_0p73_0p72_z0.0_
#set SNAPEXT = ( 001 002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 018 019 020 021 022 023)
#set SNAPNFILES = 4
#set SNAPDATA = ${PATH}/LCDM-400-run${iENSEMB}/DATA/

#-----------
# 256^3 DATA

#set PATH = /smaug/data/theorie/res/LCDM-opteron-PENN/TEST/
#set PATH = /scr/1/res/
#set SNAPBASE = LCDM-256_0p27_0p73_0p72_
#set SNAPBASE = RanCat_0.0006_LCDM-256_0p27_0p73_0p72_
#set SNAPBASE = RanCat_0.0001_LCDM-256_0p27_0p73_0p72_
#set SNAPBASE = RanCat_0.0060_LCDM-256_0p27_0p73_0p72_
#set SNAPEXT = ( 000 001 002 003 004 005 006 007 )
#set SNAPNFILES = 4
#set SNAPNFILES = 1
#set SNAPDATA = ${PATH}/lcdm-256-run1/DATA/

echo ${SNAPDATA}
echo ${SNAPBASE}
echo ${SNAPEXT[$iSNAP]}
echo ${SNAPNFILES}

set prefix   = ${SNAPDATA}/xiDualTree
set file     = Corr_${SNAPBASE}${SNAPEXT[$iSNAP]}_iRed_${iRED}_Ns_${nPartToCorr[$inPart]}_NumProcs_${numprocs}.dat
set outfile  = ${prefix}/${file}    
set timefile = ${prefix}/Timeings${file} 

if ( ! -e $outfile ) then

mpiexec -np $numprocs nice ./TwoPointCorr.MPI.2.exe <<EOF
$iRED
$nPartToCorr[$inPart]
$rMAX
$rMIN
$nbins
$nHaloBins
$iBin
${SNAPDATA}
${SNAPBASE}
${SNAPEXT[$iSNAP]}
${SNAPNFILES}
$outfile
$timefile
EOF

endif

@ iSNAP++

end

@ iENSEMB++

end

@ iRED++

end

@ inPart++

end
