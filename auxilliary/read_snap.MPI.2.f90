!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! module for reading in the GADGET-2 data 
! 
! USES MPI 
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

module snapshotMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! modules to be used---------

  ! my mpi
  USE mympi

  ! types
  USE myTYPES

!--------------------------

! defined type snapINFO

  type :: snapFILES
     integer       :: SNAPNFILES    ! Number of files per snapshot
     character*200 :: SNAPDATA      ! Directory of snapshot data
     character*200 :: SNAPBASE      ! BASE of the file names
     character*200 :: SNAPEXT       ! Extension for this snapshot
  end type snapFILES

! --- GADGET VARIABLES FOR SNAPSHOT THAT WILL BE GLOBAL --- 

  integer(I4B) ::  NN, Ntot  

  integer  :: Nall(0:5),Npart(0:5),FlagSfr,FlagFeedback,FlagCooling
  integer  :: FlagStellarAge,FlagMetals,NallHW(0:5),flagentrics
  integer  :: NpF,NumFiles
  real(SP) :: unused(15)
  real(DP) :: Massarr(0:5)
  real(DP) :: pmass,afactor,redshift
  real(DP) :: Omega0,OmegaL0,hlittle,BoxSize

!--------------------------  
! Global data arrays
  
  real(SP), allocatable    :: PartPos(:,:),PartVel(:,:)
  integer(I4B), allocatable    :: PartIDs(:) 

!--------------------------

! Memory buffering

  integer :: iBuffer

  integer*8 :: Nalloc

  real :: BufferFac=0.35

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

contains

  subroutine read_snap(sF,idat,iPos,iVel,iID,iRed)

!-------------------------

    ! sF    ==  This is the structure of the gadget snapshot file names
    ! idat  ==  Read data == 1 or Read Header Only == 0 and return  
    ! iPos  ==  Read postions == 1 or not ==0
    ! iVel  ==  Read velocities == 1 or not ==0
    ! iID   ==  Read particle IDs ==1 or not ==0
    ! iRed  ==  Redshift space: =1 Real Space; (2--4) distort in x, y, z, respectively

!-------------------------

! DECLARE SNAPSHOT VARAIBLES

    implicit none

! snap file info

    type(snapFILES) :: sf

! local file variables

    integer :: idat  ! if =1 read data, if =0 return.
    integer :: iPos  
    integer :: iVel
    integer :: iID
    integer :: iRed

    integer       :: SNAPNFILES    ! Number of files per snapshot
    character*200 :: SNAPDATA      ! Directory of snapshot data
    character*200 :: SNAPBASE      ! BASE of the file names
    character*200 :: SNAPEXT       ! Extension for this snapshot

    integer(I4B), parameter :: FILES = 50          ! number of files per snapshot
    character*260 :: filename(FILES), file1, file2

    integer(I4B) ::  i, ii, kk, noffset

!--------------------------  
! local particle data arrays
  
    real, allocatable    :: pos(:,:),vel(:,:)
    integer, allocatable    :: IDs(:)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! diagnostics

    real(SP) :: posmax1, posmax2, posmax3
    real(SP) :: posmin1, posmin2, posmin3

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! SNAPSHOT OF INTEREST

    if(myid==imaster) then
       write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
       write(*,*) '              >>> GET SNAPSHOT <<<'
       write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'      
    endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! make some local variables

    SNAPNFILES=sF%SNAPNFILES
    SNAPDATA=sF%SNAPDATA
    SNAPBASE=sF%SNAPBASE
    SNAPEXT=sF%SNAPEXT

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! find total number of particles of each type summing over snapsots

    do i=1,SNAPNFILES
         
       file1=trim(SNAPBASE)//trim(SNAPEXT)//'.'
         
       if(i.gt.1) then
          ii=int(log10(1.0*(i-1)))+1
       else
          ii=1
       endif
       
       SELECT CASE(ii)
       CASE(1) 
          write(file2,'(a,i1)') trim(file1),i-1
       CASE(2)
          write(file2,'(a,i2)') trim(file1),i-1
       END SELECT
       
       filename(i)=trim(SNAPDATA)//trim(file2)
       if(myid==imaster) then
          write(*,'(a)') filename(i)
       endif
         
    enddo
  
    if(myid==imaster) then
       write(*,*) 
       write(*,*) 'opening file:='
       write(*,'(a)') trim(filename(1))
    endif
      
! now, read in the header
  
    open (1, file=filename(1), form='unformatted', status='old')
    read (1) Npart, Massarr ,afactor, redshift, FlagSfr, FlagFeedback, Nall, &
         FlagCooling,NumFiles,BoxSize,Omega0,OmegaL0,hlittle
    close (1)
    
    if(NumFiles.ne.SNAPNFILES) then
       write(*,*) 'FUNNY! Number of files per snapshot does not match input!'
       write(*,*) 'MYID=',myid
       call MPI_ABORT(MPI_COMM_WORLD,ierr,ierror)
    endif

    if(myid==imaster) then
       write(*,*) 
       write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
       write(*,*) 'Total number of particles of each type:='
       write(*,'(6(2x,I14))') Nall(0:5)
       write(*,*) 'Snapshot Number of particles of each type'
       write(*,'(6(2x,I14))') Npart(0:5)
       write(*,*) 'Masses of particles of each type'
       write(*,'(6(2x,e14.7))') Massarr(0:5) 
       write(*,'(a,2x,f14.7,2x,a,2x,f14.7)') 'aexp:=',afactor,'redshift:=',redshift 
       write(*,*) 'BOXSIZE:=',BoxSize
       write(*,*) 'om_m0, om_DE0, h',Omega0,OmegaL0,hlittle
       write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
       write(*,*) 
    endif

!-----------------------------------------
! Allocate the memory to collect all particle info from the snapshots

    Ntot=Nall(1)

! if we only need buffer info then return
      
    if(iDat.eq.0) return

    if(iPos==1) then
       allocate(PartPos(1:3, Ntot))
    endif
    if(ivel==1) then
       allocate(PartVel(1:3, Ntot))
    endif
    if(iID==1) then
       allocate(PartIds(1:Ntot))
    endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

! Now we just read in the coordinates, and only keep those of type=1
! The array PartPos(1:3,1:Ntot) will contain all these particles

    noffset=0
  
    do i = 1, SNAPNFILES
         
       if(myid==imaster) then

          write(*,*) 'reading snapshot...  '
          write(*,'(a)') trim(filename(i))
         
       endif

! --- now, read in the file ---

       open (1, file=filename(i), form='unformatted', status='old')
       read (1) Npart, Massarr ,afactor, redshift, FlagSfr, FlagFeedback, Nall, &
            FlagCooling,NumFiles,BoxSize,Omega0,OmegaL0,hlittle
                
       NN=sum(Npart)
         
       if(myid==imaster) then
          write(*,*) 'all processors reading N particles',NN
       endif

       allocate(pos(1:3,1:NN),vel(1:3,1:NN),IDs(1:NN))
     
       read (1) pos
       read (1) vel
       read (1) IDs
     
       close (1)

!--- rescale velocities so that they are comoving peculiar velocities [km/s]

       vel = vel/(sqrt(afactor))

!--- move snapshot data into arrays ---

!-- Real or redshift space data required ---
   
!   if ired = 1 => real space positions        
!   if ired = 2 => x-axis distortion
!   if ired = 3 => y-axis distortion 
!   if ired = 4 => z-axis distortion

!   pos_s = pos_r + vel/H0

       SELECT CASE(iRed)
       CASE(2)
          
          pos(1,:)=pos(1,:)+vel(1,:)/100.0

          WHERE( pos(1,:) >= BoxSize ) pos(1,:)=pos(1,:)-BoxSize
          WHERE( pos(1,:) <  0.0 ) pos(1,:)=pos(1,:)+BoxSize
          
       CASE(3)

          pos(2,:)=pos(2,:)+vel(2,:)/100.0

          WHERE( pos(2,:) >= BoxSize ) pos(2,:)=pos(2,:)-BoxSize
          WHERE( pos(2,:) <  0.0 ) pos(2,:)=pos(2,:)+BoxSize

       CASE(4)

          pos(3,:)=pos(3,:)+vel(3,:)/100.0

          WHERE( pos(3,:) >= BoxSize ) pos(3,:)=pos(3,:)-BoxSize
          WHERE( pos(3,:) <  0.0 ) pos(3,:)=pos(3,:)+BoxSize

       END SELECT

!---Move local data to particle data arrays------------

       if(iPos==1) then
          PartPos(1:3,1+noffset:noffset+Npart(1))=pos(1:3, 1 + Npart(0):sum(Npart(0:1)))
       endif
       if(iVel==1) then
          PartVel(1:3,1+noffset:noffset+Npart(1))=vel(1:3, 1 + Npart(0):sum(Npart(0:1)))
       endif
       if(iID==1) then
          PartIds(1+noffset:noffset+Npart(1))=IDs(1 + Npart(0):sum(Npart(0:1)))
       endif
     
       noffset=noffset+Npart(1)

       if(myid==imaster) then
          write(*,*) 'freeing pos, vel, IDs'
       endif

       deallocate(pos,vel,IDs)
     
    end do

    pmass=maxval(Massarr(:))

    if(myid==imaster) then
       write(*,*) 'read snapshot data'
       write(*,*) 'mass of particles:=',pmass
    endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

! At this point, the coordinates of all particles of type 1 will
! be stored in the array PartPos(,)


    posmax1=maxval(PartPos(1,:)) ;  posmin1=minval(PartPos(1,:))
    posmax2=maxval(PartPos(2,:)) ;  posmin2=minval(PartPos(2,:))
    posmax3=maxval(PartPos(3,:)) ;  posmin3=minval(PartPos(3,:))
      
    if(myid==imaster) then
       write(*,*) 'Total number of particles found in sub snapshots:=',noffset
       write(*,*) 'Total number of particles expected:=',Ntot
       write(*,*) 'max positions',posmax1,posmax2,posmax3
       write(*,*) 'min positions',posmin1,posmin2,posmin3
       write(*,*) 
    endif

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    return
    
  end subroutine read_snap

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  subroutine generate_ransamp_snap(sF,idat,iPos,iVel,iID,iOUT,rsamp,ini)

    ! snap file info

    type(snapFILES) :: sf

    ! random number variables

    integer, parameter :: iLUX=3
    real, allocatable, dimension(:) :: rvec
    real :: rsamp

    ! local variables
    
    integer :: idat  ! if =1 read data, if =0 return.
    integer :: iPos  
    integer :: iVel
    integer :: iID
    integer :: iRed
    integer :: iRan
    integer :: iOUT

! snapshot variables
 
    integer       :: SNAPNFILES    ! Number of files per snapshot
    character*200 :: SNAPDATA      ! Directory of snapshot data
    character*200 :: SNAPBASE      ! BASE of the file names
    character*200 :: SNAPEXT       ! Extension for this snapshot
    
    character*200 :: outfile, file1

! particles

    real, allocatable,  dimension(:,:) :: PartPosRan, PartVelRan
    integer, allocatable, dimension(:) :: PartIDsRan

! make some local variables

    SNAPNFILES=sF%SNAPNFILES
    SNAPDATA=sF%SNAPDATA
    SNAPBASE=sF%SNAPBASE
    SNAPEXT=sF%SNAPEXT
    
! Initialize the random number sequences

    write(*,*) 
    write(*,*) 'initializing random number generator on processor:=',myid
    call rluxgo(iLUX,ini,0,0)

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    allocate(rvec(Ntot))

    ! generate the random numbers

    call ranlux(rvec(1:Ntot),Ntot)

    Nsamp=0
    do i=1,Ntot
       if(rvec(i)<rsamp) Nsamp=Nsamp+1
    enddo
        
    write(*,*) 'Nsamp:=',Nsamp,'myid:=',myid
    
    if (iPos==1)   allocate(PartPosRan(3,Nsamp))
    if (iVel==1)   allocate(PartVelRan(3,Nsamp))
    if (iID==1)    allocate(PartIDsRan(Nsamp))

    Nsamp=0
    do i=1,Ntot
       if(rvec(i)<rsamp) then
          Nsamp=Nsamp+1
          if(iPos==1) PartPosRan(1:3,Nsamp)=PartPos(1:3,i)
          if(iVel==1) PartVelRan(1:3,Nsamp)=PartVel(1:3,i)
          if(iID==1)  PartIDsRan(Nsamp)=PartIDs(i)
       endif
    enddo

    ! deallocate the old arrays for particle storage

    if(iPos==1) then
       deallocate(PartPos)
       allocate(PartPos(3,Nsamp))
       PartPos(:,1:Nsamp)=PartPosRan(:,1:Nsamp)
       deallocate(PartPosRan)
    endif

    if(iVel==1) then
       deallocate(PartVel)
       allocate(PartVel(3,Nsamp))
       PartVel(:,1:Nsamp)=PartVelRan(:,1:Nsamp)
       deallocate(PartVelRan)
    endif

    if(iID==1) then
       deallocate(PartIDs)
       allocate(PartIDs(Nsamp))
       PartIDs(1:Nsamp)=PartIDsRan(1:Nsamp)
       deallocate(PartIDsRan)
    endif

    ! Generated Random Catalogue

    Nall=0
    Nall(1)=Nsamp
    Npart=Nall
    NumFiles=1
    Ntot=Nsamp

    !-------------------------------------------

    if(iOUT==1) then
       
       ! output the random catalogue data 
       
       write(file1,'(a7,f6.4,a4,i4)') 'RanCat_',rsamp,'myid',myid+1000
       
       outfile=trim(SNAPDATA)//trim(file1)//'_'//trim(SNAPBASE)//trim(SNAPEXT)//'.0'
       
       write(*,'(a)') outfile
       
       open (1, file=outfile, form='unformatted', status='unknown')
       write (1) Npart, Massarr ,afactor, redshift, FlagSfr, FlagFeedback, Nall, &
            FlagCooling,NumFiles,BoxSize,Omega0,OmegaL0,hlittle
       
       write(*,*) 'writing Ntot particles',Ntot
       
       if(iPos==1) write (1) PartPos
       if(iVel==1) write (1) PartVel
       if(iID==1)  write (1) PartIDs
       
       close (1)
       
    endif

    !--------------------------------------------

    ! tidy memory
    deallocate(rvec)

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    return

  end subroutine generate_ransamp_snap

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  subroutine read_snap_and_ransamp(sF,idat,iPos,iVel,iID,iRed,rsamp,ini)

!-------------------------

    ! sF    ==  This is the structure of the gadget snapshot file names
    ! idat  ==  Read data == 1 or Read Header Only == 0 and return  
    ! iPos  ==  Read postions == 1 or not ==0
    ! iVel  ==  Read velocities == 1 or not ==0
    ! iID   ==  Read particle IDs ==1 or not ==0
    ! iRed  ==  Redshift space: =1 Real Space; (2--4) distort in x, y, z, respectively
    ! rsamp ==  sampling rate 

!-------------------------

! DECLARE SNAPSHOT VARAIBLES

    implicit none

! snap file info

    type(snapFILES) :: sf

! local file variables

    integer :: idat  ! if =1 read data, if =0 return.
    integer :: iPos  
    integer :: iVel
    integer :: iID
    integer :: iRed

    integer       :: SNAPNFILES    ! Number of files per snapshot
    character*200 :: SNAPDATA      ! Directory of snapshot data
    character*200 :: SNAPBASE      ! BASE of the file names
    character*200 :: SNAPEXT       ! Extension for this snapshot

    integer(I4B), parameter :: FILES = 50          ! number of files per snapshot
    character*260 :: filename(FILES), file1, file2

    ! random number variables

    integer, parameter :: iLUX=3
    real, allocatable, dimension(:) :: rvec
    real :: rsamp

    integer :: ini, Ns, Nsamp

    ! lcoal varaibles

    integer(I4B) ::  i, j, ii, noffset

!--------------------------  
! local particle data arrays
  
    real, allocatable    :: pos(:,:),vel(:,:)
    integer, allocatable    :: IDs(:)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! diagnostics

    real(SP) :: posmax1, posmax2, posmax3
    real(SP) :: posmin1, posmin2, posmin3

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! SNAPSHOT OF INTEREST

    if(myid==imaster) then
       write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
       write(*,*) '     >>> GET SNAPSHOT AND PRODUCE SUB-SAMPLE <<<'
       write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'      
    endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! make some local variables

    SNAPNFILES=sF%SNAPNFILES
    SNAPDATA=sF%SNAPDATA
    SNAPBASE=sF%SNAPBASE
    SNAPEXT=sF%SNAPEXT

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    write(*,*) 'initializing random number generator on processor:=',myid

    call rluxgo(iLUX,ini,0,0)

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! find total number of particles of each type summing over snapsots

    do i=1,SNAPNFILES
         
       file1=trim(SNAPBASE)//trim(SNAPEXT)//'.'
         
       if(i.gt.1) then
          ii=int(log10(1.0*(i-1)))+1
       else
          ii=1
       endif
       
       SELECT CASE(ii)
       CASE(1) 
          write(file2,'(a,i1)') trim(file1),i-1
       CASE(2)
          write(file2,'(a,i2)') trim(file1),i-1
       END SELECT
       
       filename(i)=trim(SNAPDATA)//trim(file2)
       if(myid==imaster) then
          write(*,'(a)') filename(i)
       endif
         
    enddo
  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   

! Now we just read in the coordinates, and only keep those of type=1
! The array PartPos(1:3,1:Ntot) will contain all these particles
    
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    if(myid==imaster) then
       write(*,*) 'rsamp:=',rsamp
    endif

    Ns=0

    do i = 1, SNAPNFILES
         
       open (1, file=filename(i), form='unformatted', status='old')
       read (1) Npart, Massarr ,afactor, redshift, FlagSfr, FlagFeedback, Nall, &
            FlagCooling,NumFiles,BoxSize,Omega0,OmegaL0,hlittle
       close(1)
       
       NN=sum(Npart)

       allocate(rvec(NN))

       call ranlux(rvec(1:NN),NN)

       do j=1,NN
          if(rvec(j)<rsamp) then
             Ns=Ns+1
          endif
       enddo

       deallocate(rvec)

    enddo

    if(myid==imaster) then
       write(*,*) 'Expected, number of particles sampled:=',Ns
    endif

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

! allocate memory for positions and velocities

    if(iPos==1) then
       allocate(PartPos(1:3,Ns))
    endif
    if(ivel==1) then
       allocate(PartVel(1:3,Ns))
    endif
    if(iID==1) then
       allocate(PartIds(1:Ns))
    endif

    if(myid==imaster) then
       write(*,*) 
       write(*,*) 're-initializing random number generator:=',ini
       write(*,*) 
    endif

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    call rluxgo(iLUX,ini,0,0)

    if(myid==imaster) pause 1

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

! Now read in the data and sample it

    Nsamp=0
  
    snaploop: do i = 1, SNAPNFILES
         
       if(myid==imaster) then

          write(*,*) 'reading snapshot...  '
          write(*,'(a)') trim(filename(i))
         
       endif

! --- now, read in the file ---

       open (1, file=filename(i), form='unformatted', status='old')
       read (1) Npart, Massarr ,afactor, redshift, FlagSfr, FlagFeedback, Nall, &
            FlagCooling,NumFiles,BoxSize,Omega0,OmegaL0,hlittle
                
       NN=sum(Npart)
         
       if(myid==imaster) then
          write(*,*) 'all processors reading N particles',NN
       endif

       allocate(pos(1:3,1:NN),vel(1:3,1:NN),IDs(1:NN))
     
       read (1) pos
       read (1) vel
       read (1) IDs
     
       close (1)

!--- rescale velocities so that they are comoving peculiar velocities [km/s]

       vel = vel/(sqrt(afactor))

!--- move snapshot data into arrays ---

!-- Real or redshift space data required ---
   
!   if ired = 1 => real space positions        
!   if ired = 2 => x-axis distortion
!   if ired = 3 => y-axis distortion 
!   if ired = 4 => z-axis distortion

!   pos_s = pos_r + vel/H0

       SELECT CASE(iRed)
       CASE(2)
          
          pos(1,:)=pos(1,:)+vel(1,:)/100.0

          WHERE( pos(1,:) >= BoxSize ) pos(1,:)=pos(1,:)-BoxSize
          WHERE( pos(1,:) <  0.0 ) pos(1,:)=pos(1,:)+BoxSize
          
       CASE(3)

          pos(2,:)=pos(2,:)+vel(2,:)/100.0

          WHERE( pos(2,:) >= BoxSize ) pos(2,:)=pos(2,:)-BoxSize
          WHERE( pos(2,:) <  0.0 ) pos(2,:)=pos(2,:)+BoxSize

       CASE(4)

          pos(3,:)=pos(3,:)+vel(3,:)/100.0

          WHERE( pos(3,:) >= BoxSize ) pos(3,:)=pos(3,:)-BoxSize
          WHERE( pos(3,:) <  0.0 ) pos(3,:)=pos(3,:)+BoxSize

       END SELECT

!---Now randomly sample a subset of the data-----------

       allocate(rvec(1:NN))

       ! re-generate the same random numbers as before
       call ranlux(rvec,NN)

       subsnap: do j=1,NN

          if(rvec(j)<rsamp) then
             
             Nsamp=Nsamp+1

             if(iPos==1) then
                PartPos(1:3,Nsamp)=pos(1:3,j)
             endif
             if(iVel==1) then
                PartVel(1:3,Nsamp)=vel(1:3,j)
             endif
             if(iID==1) then
                PartIds(Nsamp)=IDs(j)
             endif
             
          endif

       enddo subsnap
       
       if(myid==imaster) then
          write(*,*) 'freeing pos, vel, IDs'
       endif
       
       deallocate(pos,vel,IDs)
       deallocate(rvec)
    
    enddo snaploop

    if(myid==imaster) then
       write(*,*) 'Actual, number of particles sampled:=',Nsamp
    endif

    if(Ns.ne.Nsamp) then
       write(*,*) 'NS and Nsamp are not equal: ABORT. Myid==',myid
       CALL MPI_ABORT(MPI_COMM_WORLD,ierr,ierror)
    endif

    pmass=maxval(Massarr(:))

    if(myid==imaster) then
       write(*,*) 'read and sampled the snapshot data'
       write(*,*) 'mass of particles:=',pmass
    endif

    ! redefine Ntot as the number of sampled points

    Ntot=Nsamp

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

! At this point, the coordinates of all particles of type 1 will
! be stored in the array PartPos(,)

    posmax1=maxval(PartPos(1,:)) ;  posmin1=minval(PartPos(1,:))
    posmax2=maxval(PartPos(2,:)) ;  posmin2=minval(PartPos(2,:))
    posmax3=maxval(PartPos(3,:)) ;  posmin3=minval(PartPos(3,:))
      
    if(myid==imaster) then
       write(*,*) 'Total number of particles found in sub snapshots:=',noffset
       write(*,*) 'Total number of particles expected:=',Ntot
       write(*,*) 'max positions',posmax1,posmax2,posmax3
       write(*,*) 'min positions',posmin1,posmin2,posmin3
       write(*,*) 
    endif

    call MPI_BARRIER(MPI_COMM_WORLD,ierr)

    return
    
  end subroutine read_snap_and_ransamp

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end module snapshotMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
