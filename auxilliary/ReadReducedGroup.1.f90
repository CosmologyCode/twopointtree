!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

module HaloTYPE

  USE nrtype

  type Halo

     integer  :: HN                       ! number of particles in the halo                  
     real(DP) :: Hmass                    ! Halo Mass fof=0.2 [10**10 Mo/h]

     real(DP), dimension(3) :: HPos       ! centre of mass position [Mpc/h]
     real(DP), dimension(3) :: HVel       ! centre of mass velocity [km/s]

     real(DP) :: Hrad                     ! radius of halo = mean of last 4 part.
     real(DP) :: Hradhalf                 ! Half-mass radius
     real(DP) :: HDisp1D                  ! 1D velocity dispersion [km/s]

     real(DP), dimension(6) :: HInert     ! Inertia matrix: 
                                          ! I11, I22, I33, I12, I13, I23
  end type Halo

  type(halo), allocatable, dimension(:) :: HaloFINAL, HaloIC

end module HaloTYPE

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subroutine ReadReducedGroups(sF)

! Read a reduced groups catalogue

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! VARIABLE DECLARATIONS
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! types

  USE myTYPES

! Halo type module

  USE HaloTYPE

! Gadget snapshot 

  USE snapshotMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  implicit none

  ! Gadget files:
  type(snapFILES) :: sF

  character*200 :: HALODATA      ! Directory of snapshot data
  character*200 :: HALOBASE      ! BASE of the file names
  character*200 :: HALOFILE

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  integer :: TotNgroups

  character*120 :: infile, file, datadir

  integer :: iGroup

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! DATA DIRECTORY FOR THE REDUCED HALO CATALOGUES

  HALODIR=sF%SNAPDATA
  HALODIR=trim(HALODIR)//'reducedGROUPS'

  ! FILE NAME

  HALOBASE='GRPS_'//trim(SNAPBASE)//trim(SNAPEXT)

  HALOFILE=trim(HALODIR)//trim(HALOBASE)

  if(myid==imaster) then
     write(*,*) 'All processors reading...'
     write(*,'(a)') infile
  endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  open(1,file=HALOFILE,status='old',form='unformatted')
  read(1) TotNgroups

  if(myid==imaster) then
     write(*,*) TotNgroups
  endif
    
  allocate(HaloFINAL(TotNgroups))

  do iGroup=1,TotNgroups
     read(1) HaloFINAL(iGroup)!, HaloIC(i)
  enddo
  close(1)

  return

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end subroutine ReadReducedGroups

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
