!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

module myMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! include mpi module

  USE MPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! MPI VARIABLES

  implicit none
  
  integer :: ierror                     ! error flag 
  integer :: ierr                     ! error flag 
  integer :: numprocs                 ! number of processors
  integer :: myid                     ! processors ID
  integer :: imaster, islave          ! ID of master and slaves
  integer :: iTAG                     ! Tag of the MPI message
  integer :: status(MPI_STATUS_SIZE)  ! status array for passing tags etc..
  
! a local MPI structure 
  
  integer, dimension(4) :: blockcounts  ! number of blocks in the structure 
  integer, dimension(4) :: types        ! MPI data types in the structure 
  integer, dimension(4) :: displace     ! displacements in bytes

  integer :: MPI_GADGETFILE_TYPE        ! the new MPI type

  integer, parameter :: NUMPROCSMAX = 100

end module myMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
