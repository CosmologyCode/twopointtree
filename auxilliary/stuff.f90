!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! single tree walk correlation function

  ijump=1
  if(ijump==0) then

  ini=1
  rsamp=dble(1)/dble(Ntot)
  rsamp=1.0

  call cpu_time(t0)
  call SingleTreeTwoPointRanSamp(RootPT1,rMIN,rMAX,rbin,Ncorr,ini,rsamp,Nsamp)
  call cpu_time(t1)

  write(*,*) 'time taken to compute the Single Tree-walk correlations:='
  write(*,*) t1-t0

  do i=1,nbins

     rr=rMIN+(i-0.5)*rbin
     rr1=rMIN+(i-1)*rbin
     rr2=rMIN+i*rbin

     Vshell=4.0/3.0*pi*(rr2**3-rr1**3)

     NRanCorr(i)=nint((vshell/vol)*Nsamp*Ntot)

     corr(i)=dble(Ncorr(i))/dble(NRanCorr(i))-1.0

     Rad(i)=rr
     Rad1(i)=rr1
     Rad2(i)=rr2

     write(*,10) i,rr,corr(i),Ncorr(i),NRanCorr(i)

  enddo

!  outfile=trim(SNAPDATA)//'xiSingleTree/CorrTestSingleTree.dat'
  outfile='CorrTestSingleTree.dat'
  open(1,file=outfile,status='unknown',form='formatted')
  do i = 1, nbins
     write(1,11) Rad(i), Rad1(i), Rad2(i), NCorr(i), NRanCorr(i), corr(i)
  enddo
  close(1)

  endif
  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! Brute Force Correlation function estimate
  
  ijump=1
  if(ijump==0) then

  ini=1
  rsamp=dble(50)/dble(Ntot)
  rsamp=1.0

  call cpu_time(t0)
  call BruteForceTwoPointRanSamp(RootPT1,rMIN,rMAX,rbin,Ncorr,ini,rsamp,Nsamp)
  call cpu_time(t1)

  write(*,*) 'time taken to compute the Brute Force correlations:='
  write(*,*) t1-t0

  do i=1,nbins

     rr=rMIN+(i-0.5)*rbin
     rr1=rMIN+(i-1)*rbin
     rr2=rMIN+i*rbin

     Vshell=4.0/3.0*pi*(rr2**3-rr1**3)

     NRanCorr(i)=nint((vshell/vol)*Nsamp*Ntot)

     corr(i)=dble(Ncorr(i))/dble(NRanCorr(i))-1.0

     Rad(i)=rr
     Rad1(i)=rr1
     Rad2(i)=rr2

     write(*,10) i,rr,corr(i),Ncorr(i),NRanCorr(i)

  enddo

  outfile='CorrTestBruteForce.dat'
  open(1,file=outfile,status='unknown',form='formatted')
  do i = 1, nbins
     write(1,11) Rad(i), Rad1(i), Rad2(i), NCorr(i), NRanCorr(i), corr(i)
  enddo
  close(1)

  endif
