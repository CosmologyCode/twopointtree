!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

module Haloes

  USE myTYPES

  type Halo

     integer  :: HN                       ! number of particles in the halo                  
     real(DP) :: Hmass                    ! Halo Mass fof=0.2 [10**10 Mo/h]

     real(DP), dimension(3) :: HPos       ! centre of mass position [Mpc/h]
     real(DP), dimension(3) :: HVel       ! centre of mass velocity [km/s]

     real(DP) :: Hrad                     ! radius of halo = mean of last 4 part.
     real(DP) :: Hradhalf                 ! Half-mass radius
     real(DP) :: HDisp1D                  ! 1D velocity dispersion [km/s]

     real(DP), dimension(6) :: HInert     ! Inertia matrix: 
                                          ! I11, I22, I33, I12, I13, I23
  end type Halo

  type(halo), allocatable, dimension(:) :: HaloFINAL, HaloIC


  integer :: TotNgroups         ! Total number of haloes in the mass bin

  real, allocatable, dimension(:,:) :: HaloPos    ! Halo Positions


end module Haloes

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subroutine ReadReducedGroups(sF,iMass,iRed)

! Read a reduced groups catalogue

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! VARIABLE DECLARATIONS
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! types

  USE myTYPES

! Halo type module

  USE Haloes

! Gadget snapshot 

  USE snapshotMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  implicit none

  ! Gadget files:
  type(snapFILES) :: sF

  character*200 :: HALODATA      ! Directory of snapshot data
  character*200 :: HALOBASE      ! BASE of the file names
  character*200 :: HALOFILE

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  integer :: iMass               ! select a mass bin : 
                                 ! there are 6 bins with S/N = 20 
                                 ! iMASS=1 highest ; iMASS=6 = lowest
 
  integer :: iRed                ! =1 no redshift space distortions
                                 ! = (2,3,4) apply distortion in (x-,y-,z-) 
  
! local variables

  integer :: iGroup, nh          
 
  real(DP) :: xm1, xm2, xm               

  real(DP), parameter :: MassUNIT = 1.0d10

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! DATA DIRECTORY FOR THE REDUCED HALO CATALOGUES

  HALODATA=sF%SNAPDATA
  HALODATA=trim(HALODATA)//'reducedGROUPS/'

  ! FILE NAME

  HALOBASE='GRPS_'//trim(sF%SNAPBASE)//trim(sF%SNAPEXT)

  HALOFILE=trim(HALODATA)//trim(HALOBASE)

  if(myid==imaster) then
     write(*,*) 'All processors reading...'
     write(*,'(a)') HALOFILE
  endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! HALO MASS RANGES TO COMPUTE CORRELATION FUNCTIONS FOR

  if(iMASS == 1) then
     xm1=10**14.0/(MassUNIT)
     xm2=10**16.0/(MassUNIT)
  elseif(iMASS==2) then
     xm1=10**13.7/(MassUNIT)
     xm2=10**14.0/(MassUNIT)
  elseif(iMASS==3) then
     xm1=10**13.5/(MassUNIT)
     xm2=10**13.7/(MassUNIT)
  elseif(iMASS==4) then
     xm1=10**13.3/(MassUNIT)
     xm2=10**13.5/(MassUNIT)
  elseif(iMASS==5) then
     xm1=10**13.14/(MassUNIT)
     xm2=10**13.3/(MassUNIT)
  elseif(iMASS==6) then
     xm1=10**13.0/(MassUNIT)
     xm2=10**13.14/(MassUNIT)
  endif

  if(myid==imaster) then
     write(*,*) 'Halo mass cuts:=',xm1,xm2
  endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  open(1,file=HALOFILE,status='old',form='unformatted')

  read(1) TotNgroups

  if(myid==imaster) then
     write(*,*) 'TOTAL NUMBER OF HALOES IN CATAOLGUE:=',TotNgroups
  endif
    
  ! determine the number of haloes in the mass bin

  allocate(HaloFINAL(TotNgroups))

  nh=0
  do iGroup=1,TotNgroups
     read(1) HaloFINAL(iGroup)
     xm=HaloFINAL(iGroup)%Hmass
     if(xm >= xm1 .and. xm < xm2 ) then
        nh=nh+1
     endif
  enddo
  close(1)


  allocate(HaloPos(3,nh))

  nh=0
  do iGroup=1,TotNgroups

     xm=HaloFINAL(iGroup)%Hmass

     if(xm >= xm1 .and. xm < xm2 ) then

        nh=nh+1
        HaloPos(1:3,nh)=HaloFINAL(iGroup)%HPos(1:3)
        
        !!!!!! WORRY : gadget velocities are sqrt(a) * vp , so we may need
        !!!!!! to divide by sqrt(a) to get normal velocuities before 
        !!!!!! transforming to z-space

        if(iRed.eq.2) then           
           HaloPos(1,nh)=HaloPos(1,nh)+  & 
                HaloFINAL(iGroup)%HVel(1)/(100.0*sqrt(afactor))
        elseif(iRed.eq.3) then           
           HaloPos(2,nh)=HaloPos(2,nh)+  & 
                HaloFINAL(iGroup)%HVel(2)/(100.0*sqrt(afactor))
        elseif(iRed.eq.4) then           
           HaloPos(3,nh)=HaloPos(3,nh)+  & 
                HaloFINAL(iGroup)%HVel(3)/(100.0*sqrt(afactor))
        endif

     endif
  enddo

  if(myid==imaster) then
     write(*,*) 'TOTAL NUMBER OF HALOES BEFORE CUT:=',TotNgroups
  endif

  TotNgroups = nh

  if(myid==imaster) then
     write(*,*) 'TOTAL NUMBER OF HALOES USED:=',TotNgroups
  endif

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! RECALL that you screwed up the periodic boundary on the halo catalogue 
  ! This can be fixed by just making sure that all haloes fit in the box

  where (HaloPos(:,:) < 0.0 )
     HaloPos(:,:)=HaloPos(:,:)+BoxSize
  end where

  where (HaloPos(:,:) >= BoxSize  )
     HaloPos(:,:)=HaloPos(:,:)-BoxSize
  end where

  ! clean up memory
  deallocate(HaloFINAL)

  return

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end subroutine ReadReducedGroups

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
