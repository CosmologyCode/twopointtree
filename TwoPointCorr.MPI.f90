!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! Makefile for TwoPointCorr.MPI.1 
! 
! Written by:
! Robert E. Smith, 
! Institute for Theoretical Physics,
! University of Zurich (2007)
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! REFERENCES:
! 
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! The routine for generation of the KD-tree was based upon:
!
! Michael Buhl and Matthew Kennel,
! Institute for Nonlinear Science (2002).
! "KDTREE 2: Fortran 95 and C++ software to efficiently search 
! for near neighbors in a multi-dimensional Euclidean space"
! http://arxiv.org/abs/physics/0408067
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! The two-point correlation function routines were based upon:
!
! "Fast Algorithms and Efficient Statistics: N-point correlation functions"
! Andrew Moore (CMU), Andy Connolly (UPitt), Chris Genovese (CMU), Alex Gray (CMU),
! Larry Grone (CMU), Nick Kanidoris II (CMU), Robert Nichol (CMU), Jeff Schneider (CMU), 
! Alex Szalay (JHU), Istvan Szapudi (CITA), Larry Wasserman (CMU),
! in Proceedings of MPA/MPE/ESO Conference "Mining the Sky", 2000, Garching, Germany 
! astro-ph/0012333  
!
! "The skewness of the aperture mass statistic"
! Michael Jarvis, Gary Bernstein, Bhuvnesh Jain, 2004, 
! Monthly Notices of the Royal Astronomical Society, 352, 338.
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!
! The random number generation is performed using the:
!
! Subtract-and-borrow random number generator proposed by
! Marsaglia and Zaman, implemented by F. James with the
! name RCARRY in 1991, and later improved by Martin Luescher          
! in 1993 to produce "Luxury Pseudorandom Numbers". 
!
! Fortran 77 coded by F. James, 1993
! references: 
! M. Luscher, Computer Physics Communications  79 (1994) 100
! F. James, Computer Physics Communications 79 (1994) 111
!
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

program TwoPointCorrMPI

  ! THIS IS AN MPI CODE..........
  ! compile with mpif90 

  ! The Two-Point correlation function routines are: 
  ! 1: DualTreeTwoPoint; 
  ! 2: SingleTreeTwoPoint; 
  ! 3: SingleTreeTwoPointRanSamp; 
  ! 4: BruteForceTwoPointRanSamp

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! LOAD MODULES....

  ! my mpi module -- this also includes the mpich2 module
  USE myMPI

  ! Numerical recipes
  USE myTYPES

  ! Gadget snapshot 
  USE snapshotMPI

  ! Halo data 
  USE Haloes

  ! kd-tree
  USE kd_treeMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  implicit none

  ! useful variables

  integer :: len1, len2, len3, ll

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! PROGRAM VARIABLES.........

  ! Gadget files:
  type(snapFILES) :: sF

  integer       :: SNAPNFILES    ! Number of files per snapshot
  character*200 :: SNAPDATA      ! Directory of snapshot data
  character*200 :: SNAPBASE      ! BASE of the file names
  character*200 :: SNAPEXT       ! Extension for this snapshot

  ! Gadget snapshot flags 
  integer :: idat                ! =1 read data; =0 get header
  integer :: iPos                ! =1 read positions; =0 do not.
  integer :: iVel                ! =1 read velocities; =0 do not.
  integer :: iID                 ! =1 read the ID's; =0 do not.
  integer :: iRed                ! =1 no redshift space distortions
                                 ! = (2,3,4) apply distortion in (x-,y-,z-) 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! HALO CATALOGUE VARIABLES

  integer :: iMass               ! select a mass bin 
                                 ! 1 => (1.0*10^13 <= M [Mo/h] < 5.0*10^13 )
                                 ! 1 => (5.0*10^13 <= M [Mo/h] < 1.0*10^14 )
                                 ! 1 => (1.0*10^14 <= M [Mo/h])


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! Variables for constructing random sample of data

  real :: rsamp                  ! Sampling rate
  integer :: ini                 ! initial random seed
  integer :: nPartToCorr         ! number of particles to correlate

  ! If generating a random catalogue from Gadget

  integer :: iRan                ! =1 generate a random catalogue 
  integer :: iOUT                ! =1 write out the random catalogue; =0 do not

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! Master Tree Nodes and their pointers 

! If computing auto-correlation functions, only need one master & pointer 
! If computing cross-correlation functions, then second tree root needed.

  Type(tree_master_record), target  :: TreeRoot1       
  Type(tree_master_record), pointer :: RootPT1  

  Type(tree_master_record), target  :: TreeRoot2
  Type(tree_master_record), pointer :: RootPT2

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! correlation function variables::

  real :: rbin, rMAX, rMIN

  integer :: iBin                ! Radial Binning: linear =0; logarithmic = 1 

  integer :: nbins, nHaloBins

  integer, parameter :: nbinsMAX=200, nHaloBinsMAX=10

  integer, dimension(nHaloBinsMAX) :: nhalo

  real*8, dimension(nbinsMAX) :: Rad, Rad1, Rad2

  real*8, dimension(nbinsMAX)           :: NcorrMM, NRanCorrMM
  real*8, dimension(nbinsMAX,nHaloBinsMAX) :: NcorrHM, NRanCorrHM
  real*8, dimension(nbinsMAX,nHaloBinsMAX) :: NcorrHH, NRanCorrHH

  real, dimension(nbinsMAX)           :: CorrMMBar, CorrMMVar, CorrMMSig
  real, dimension(nbinsMAX,nHaloBinsMAX) :: CorrHMBar, CorrHMVar, CorrHMSig
  real, dimension(nbinsMAX,nHaloBinsMAX) :: CorrHHBar, CorrHHVar, CorrHHSig

  real, target, dimension(nbinsMAX) :: MyCorrMM
  real, target, dimension(nbinsMAX,nHaloBinsMAX) :: MyCorrHM
  real, target, dimension(nbinsMAX,nHaloBinsMAX) :: MyCorrHH

  Type :: CorrPT
     real, pointer, dimension(:)   :: MM
     real, pointer, dimension(:,:) :: HM
     real, pointer, dimension(:,:) :: HH
  end Type CorrPT

  Type(CorrPT) :: MyCorr,NewCorr

  real, allocatable, dimension(:,:)   :: CorrMM
  real, allocatable, dimension(:,:,:) :: CorrHM
  real, allocatable, dimension(:,:,:) :: CorrHH

  integer :: MPI_CORR_TYPE

  integer :: Nsamp

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! TIMINGS VARIABLES
  
  real*8, dimension(nbinsMAX) :: timeMM
  real*8, dimension(nbinsMAX,nHaloBinsMAX) :: timeHM, timeHH

  real*8 :: t1, t0

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! local variables

  integer :: i, j, ijump

  real :: aa, vol, rr, rr1, rr2, vshell
  
  character :: outfile*260, timefile*260, achar*1, achar2*100

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! INIITALIZE MPI

  call MPI_INIT(ierr) 

  ! get ranks
  call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)

  ! get numprocessors
  call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

  ! set the id of the imaster processor
  imaster=0 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(myid == imaster) then

     write(*,*) '*****************************************************************'
     write(*,*) 'RUNNING MPI CODE TO COMPUTE CORRELATION FUNCTIONS USING A kD-TREE'
     write(*,*) '*****************************************************************'
     write(*,*) 'Number of processors in use:=',numprocs
     write(*,*) 'Master is ranked as:=',imaster
     write(*,*) 
     write(*,*) '====: Enter user specific inputs :===='

     ! --- inputs ---

     write(*,*) 'input: iRED : Real == 1 or Redshift space Correlation == 2,3,4 (x,y,z)'
     read(*,*) iRED
     write(*,*) iRED

     write(*,*) 'input: The number of particles that you wish to correlate:='
     read(*,*) nPartToCorr
     write(*,*) nPartToCorr

     write(*,*) 'input: rad max:='
     read(*,*) rMAX
     write(*,*) rMAX
     
     write(*,*) 'input: rad min:='
     read(*,*) rMIN
     write(*,*) rMIN

     write(*,*) 'input: nbins [<=200]:='
     read(*,*) nbins
     write(*,*) nbins
     
     write(*,*) 'input: nHaloBins [<=3]'
     read(*,*) nHaloBins
     write(*,*) nHaloBins

     write(*,*) 'input: radial binning: linear, iBin = 0; iBin=1 logarithmic'
     read(*,*) iBin
     write(*,*) iBin

     write(*,*) 
     write(*,*) 'input: SNAPSHOT INFO..........'
     write(*,*) 

     ! snapshot SNAPDATA
     write(*,*) 'input:  snapshot DATA:='
     read(*,'(a)') SNAPDATA
     write(*,'(a)') SNAPDATA
     
     ! snapshot SNAPBASE
     write(*,*) 'input:  snapshot BASE:='
     read(*,'(a)') SNAPBASE
     write(*,'(a)') SNAPBASE

     ! snapshot SNAPEXT     
     write(*,*) 'input:  snapshot EXT:='
     read(*,'(a)') SNAPEXT
     write(*,'(a)') SNAPEXT

     ! snapshot SNAPNFILES
     write(*,*) 'input:  snapshot NFILES:='
     read(*,*) SNAPNFILES
     write(*,*) SNAPNFILES

     ! outfile
     write(*,*) 'input:  name of output file for correlation functions:='
     read(*,'(a)') outfile
     write(*,'(a)') outfile

     ! timings file
     write(*,*) 'input:  name of timings file for correlation functions:='
     read(*,'(a)') timefile
     write(*,'(a)') timefile
     
     !------------------

! set the file type

     sf%SNAPDATA = SNAPDATA
     sf%SNAPBASE = SNAPBASE
     sf%SNAPEXT = SNAPEXT
     sf%SNAPNFILES = SNAPNFILES
     
  endif
  
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! BROADCAST ALL INPUT VARAIBLES

  if(myid==imaster) then
     write(*,*) 
     write(*,*) 'BROADCASTING VARAIBLES....'
     write(*,*) 
  endif
  
  ! Pass iRed
  call MPI_BCAST(iRed,1,MPI_INTEGER,imaster, MPI_COMM_WORLD, ierr)

  ! Pass nPartToCorr
  call MPI_BCAST(nPartToCorr,1,MPI_INTEGER,imaster, MPI_COMM_WORLD, ierr)

  ! Pass rMAX
  call MPI_BCAST(rMAX,1,MPI_REAL,imaster, MPI_COMM_WORLD, ierr)

  ! Pass rMIN
  call MPI_BCAST(rMIN,1,MPI_REAL,imaster, MPI_COMM_WORLD, ierr)

  ! Pass nbins
  call MPI_BCAST(nbins,1,MPI_INTEGER,imaster, MPI_COMM_WORLD, ierr)

  ! Pass nHaloBins
  call MPI_BCAST(nHaloBins,1,MPI_INTEGER,imaster, MPI_COMM_WORLD, ierr)

  ! Pass iBIN
  call MPI_BCAST(iBin,1,MPI_INTEGER,imaster, MPI_COMM_WORLD, ierr)

  ! Pass outfile
  len1=len(outfile)
  call MPI_BCAST(outfile,len1,MPI_CHARACTER,imaster, MPI_COMM_WORLD, ierr)

  ! Pass Timefile
  len1=len(timefile)
  call MPI_BCAST(timefile,len1,MPI_CHARACTER,imaster, MPI_COMM_WORLD, ierr)

  ! Pass Gadget file names 

  ! ..create an MPI structure to do this

  ! ..first get the lengths of the file strings
  len1=len(sF%SNAPDATA)
  len2=len(sF%SNAPBASE)
  len3=len(sF%SNAPEXT)

  ! ..generate the size of the blocks for the type-map
  blockcounts(1:4)=(/1,len1,len2,len3/)
  
  ! ..Get the addresses of the variables to pass
  call MPI_ADDRESS(sF%SNAPNFILES,displace(1), ierr) 
  call MPI_ADDRESS(sF%SNAPDATA  ,displace(2), ierr) 
  call MPI_ADDRESS(sF%SNAPBASE  ,displace(3), ierr) 
  call MPI_ADDRESS(sF%SNAPEXT   ,displace(4), ierr) 

  ! ..Provide the list of types to pass
  types(1) = MPI_INTEGER
  types(2) = MPI_CHARACTER
  types(3) = MPI_CHARACTER
  types(4) = MPI_CHARACTER

  displace(1:4)=displace(1:4)-displace(1)  
  call MPI_TYPE_STRUCT(4 ,blockcounts, displace, types, MPI_GADGETFILE_TYPE, ierr)
  call MPI_TYPE_COMMIT(MPI_GADGETFILE_TYPE, ierr)  

  ! Broadcast the information to all processors
  call MPI_BCAST(sF,1, MPI_GADGETFILE_TYPE, imaster, MPI_COMM_WORLD, ierr)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! LOAD SIMULATION DATA.......
  ! AND GENERATE A RANDOM CATALOGUE ON THE FLY

  if(myid == imaster) then
     write(*,*) 
     write(*,*) 'READ DATA ON ALL PROCESSORS, SAMPLING ON-THE-FLY....'
     write(*,*) 
  endif

  ! Get the simulation parameters
  idat=0; iPos=0; iVel=0; iID=0
  call read_snap(sF,idat,iPos,iVel,iID,iRed)  

  ! read the data and randomly sample

  rsamp=dble(nPartToCorr)/dble(Ntot)   ! sampling factor

  ini = myid + 1                       ! initialization of random numbers

  idat=1; iPos=1; iVel=0 ; iID=0
  call read_snap_and_ransamp(sF,idat,iPos,iVel,iID,iRed,rsamp,ini) 
     
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(myid == imaster) then
     write(*,*) 
     write(*,*) '               >>> Done the read <<<'
     write(*,*) 
  endif

  ! some useful simulation variables

  vol=BoxSize**3  

  if(iBin==1) then 
     ! logarithmic binning
     rMAX=log10(rMAX)
     rMIN=log10(rMIN)
  endif

  rbin=(rMAX-rMIN)/(nbins*1.0)

  if(myid==imaster) then
     write(*,*) rMAX,rMIN,rbin
  endif
     
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! --- DARK MATTER TREE ---

  if(myid == imaster ) then
     write(*,*) 
     write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
     write(*,*) '     >>> MATTER - MATTER CORRELATION FUNCTION <<<'
     write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
     write(*,*) 
     write(*,*) 'generate a k-d tree on each processor' 
     write(*,*) 
  endif

  ! MPI timeings
  t0 = MPI_WTIME() 
  TreeRoot1=create_tree(PartPos(:,:),BoxSize)
  t1 = MPI_WTIME()

  if(myid == imaster) then
     write(*,*) 
     write(*,*) 'Time taken to grow the trees:=',t1-t0
     write(*,*) 
  endif

  call MPI_BARRIER(MPI_COMM_WORLD,ierr)  

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! DARK MATTER tree 

  RootPT1 => TreeRoot1

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! >>> COMPUTE MATTER-MATTER CORRELATION FUNCTIONS <<<

  t0 = MPI_WTIME() 
  call DualTreeTwoPoint(RootPT1,RootPT1,iBin,rMIN,rMAX,& 
       rbin,NcorrMM(1:nbins),timeMM(1:nbins))
  t1 = MPI_WTIME()

  if(myid == imaster) then
     write(*,*) 
     write(*,*) 'Total Time taken to compute MATTER-MATTER correlation:=',t1-t0
     write(*,*) 
  endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(myid==imaster) then
  write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
  write(*,*) '      >>> COMPUTE HALO CORRELATION FUNCTIONS <<<'
  write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
  write(*,*) 
  endif

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! LOAD THE DARK MATTER HALO DATA

  do j=1,nHaloBins

     if(myid == imaster) then
        write(*,*) 
        write(*,*) 'HALO BIN:=',j
        write(*,*) 
     endif

     iMASS=j
     call ReadReducedGroups(sF,iMASS,iRed)  

     nhalo(j)=TotNgroups

     ! GEERATE HALO tree 
     
     TreeRoot2=create_tree(HaloPos(:,:),BoxSize)
     
     RootPT2 => TreeRoot2

     ! COMPUTE HALO-MATTER CROSS-CORRELATION FUNCTIONS  

     if(myid==imaster) then
        write(*,*) 
        write(*,*) '>>> computing HALO-MATTER CROSS-CORRELATION <<< '
        write(*,*) 
     endif

     t0 = MPI_WTIME() 
     call DualTreeTwoPoint(RootPT1,RootPT2,iBin,rMIN,rMAX,rbin, & 
          NcorrHM(1:nbins,j),timeHM(1:nbins,j))
     t1 = MPI_WTIME() 

     if(myid==imaster) then
        write(*,*) 
        write(*,*) 'time taken:=',t1-t0
        write(*,*) 
     endif

     call MPI_BARRIER(MPI_COMM_WORLD,ierr)

     ! COMPUTE HALO-HALO CORRELATION FUNCTIONS  

     if(myid==imaster) then
        write(*,*) 
        write(*,*) '>>> computing HALO-HALO CORRELATION <<<'
        write(*,*) 
     endif

     t0 = MPI_WTIME() 
     call DualTreeTwoPoint(RootPT2,RootPT2,iBin,rMIN,rMAX,rbin, &
          NcorrHH(1:nbins,j),timeHH(1:nbins,j))
     t1 = MPI_WTIME() 

     if(myid==imaster) then
        write(*,*) 
        write(*,*) 'time taken:=',t1-t0
        write(*,*) 
        write(*,*) '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
     endif

  enddo

  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! every processor now estimates the correlation functions: 
  
  do i=1,nbins
     
     rr=rMIN+(i-0.5)*rbin
     rr1=rMIN+(i-1)*rbin
     rr2=rMIN+i*rbin

     if(iBin==1) then
        rr=10**rr
        rr1=10**rr1
        rr2=10**rr2
     endif
     
     Vshell=4.0/3.0*pi*(rr2**3-rr1**3)
     
     ! mass-mass correlation

     NRanCorrMM(i)=(vshell/vol)*dble(Ntot)*dble(Ntot)

     MyCorrMM(i)=(NcorrMM(i)/NRanCorrMM(i))-1.0

     do j=1,nHaloBins

        ! halo-mass cross-correlation
        NRanCorrHM(i,j)=(vshell/vol)*dble(nhalo(j))*dble(Ntot)
        MyCorrHM(i,j)=(NcorrHM(i,j)/NRanCorrHM(i,j))-1.0
        
        ! halo-halo correlation
        NRanCorrHH(i,j)=(vshell/vol)*dble(nhalo(j))*dble(nhalo(j))
        MyCorrHH(i,j)=(NcorrHH(i,j)/NRanCorrHH(i,j))-1.0

     enddo

     Rad(i)=rr
     Rad1(i)=rr1
     Rad2(i)=rr2

  enddo

  MyCorr%MM => MyCorrMM
  MyCorr%HM => MyCorrHM
  MyCorr%HH => MyCorrHH

  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ! SEND all of the correlation functions to master
  ! --- create an MPI structure to pass this information

  ! generate the size of the blocks for the type-map

  if(myid==imaster) then
     write(*,*) 'GATHERING RESULTS......'
  endif

  len1=size(MyCorr%MM)
  len2=size(MyCorr%HM)
  len3=size(MyCorr%HH)

  blockcounts(1:3)=(/len1,len2,len3/)
  
  ! Get the addresses of the variables to pass
  call MPI_ADDRESS(MyCorr%MM,displace(1), ierr) 
  call MPI_ADDRESS(MyCorr%HM,displace(2), ierr) 
  call MPI_ADDRESS(MyCorr%HH,displace(3), ierr) 

  ! Provide the list of types to pass
  types(1) = MPI_REAL
  types(2) = MPI_REAL
  types(3) = MPI_REAL

  displace(1:3)=displace(1:3)-displace(1)  

  call MPI_TYPE_STRUCT(3 ,blockcounts, displace, types, MPI_CORR_TYPE, ierr)
  call MPI_TYPE_COMMIT(MPI_CORR_TYPE, ierr)  

  ! GATHER CORRELATIONS

  ! gather together all estimates from all processors

  allocate(CorrMM(numprocs,nbins))
  allocate(CorrHM(numprocs,nbins,nHaloBins))
  allocate(CorrHH(numprocs,nbins,nHaloBins))

  if(myid==imaster) then
     
     ! master places their estimate in bin

     CorrMM(1,1:nbins)=MyCorr%MM(1:nbins)
     CorrHM(1,1:nbins,1:nHaloBins)=MyCorr%HM
     CorrHH(1,1:nbins,1:nHaloBins)=MyCorr%HH

     ! master receives estimates from slaves

     do i=1,numprocs-1

        call MPI_RECV(MyCorr%MM, 1, MPI_CORR_TYPE, MPI_ANY_SOURCE, & 
             MPI_ANY_TAG, MPI_COMM_WORLD, status, ierr)

        iTAG=status(MPI_TAG)
        CorrMM(iTAG,1:nbins)=MyCorr%MM
        CorrHM(iTAG,1:nbins,1:nHaloBins)=MyCorr%HM
        CorrHH(iTAG,1:nbins,1:nHaloBins)=MyCorr%HH

     enddo

  else

     ! slaves send in their estimates

     iTAG=myid+1
     
     call MPI_SSEND(MyCorr%MM, 1, MPI_CORR_TYPE, imaster, & 
          iTAG, MPI_COMM_WORLD, ierr)

  endif

  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! master computes the mean correlation function

  if(myid==imaster) then
          
     write(*,*) '! master computes the mean correlation function'     

     !----------------------------------------------
     
     ! compute the mean correlation functions

     !-----------------------------------------
     ! MATTER - MATTER

     CorrMMBar(:)=sum(CorrMM,dim=1)/(numprocs*1.0)     

     CorrMMVar(:)=sum(CorrMM*CorrMM,dim=1)/(numprocs*1.0)

     CorrMMVar(:)=CorrMMVar(:)-CorrMMBar(:)**2

     CorrMMSig(:)=sqrt(max(1.0e-10,CorrMMVar))

     !-----------------------------------------
     ! HALO - MATTER CROSS CORRELATION FUNCTION

     CorrHMBar(:,:)=sum(CorrHM,dim=1)/(numprocs*1.0)     

     CorrHMVar(:,:)=sum(CorrHM*CorrHM,dim=1)/(numprocs*1.0)

     CorrHMVar(:,:)=CorrHMVar(:,:)-CorrHMBar(:,:)**2

     CorrHMSig(:,:)=sqrt(max(1.0e-10,CorrHMVar))

     !-----------------------------------------
     ! HALO - HALO CORRELATION FUNCTION 

     CorrHHBar(:,:)=sum(CorrHH,dim=1)/(numprocs*1.0)     

     CorrHHVar(:,:)=sum(CorrHH*CorrHH,dim=1)/(numprocs*1.0)

     CorrHHVar(:,:)=CorrHHVar(:,:)-CorrHHBar(:,:)**2

     CorrHHSig(:,:)=sqrt(max(1.0e-10,CorrHHVar))
        
     !----------------------------------------------
     ! output the timings data file

     open(1,file=timefile,status='unknown',form='formatted')
     write(1,*) numprocs,nbins
     do i = 1, nbins
        write(1,10) Rad(i), timeMM(i), timeHM(i,1:nHaloBins),timeHH(i,1:nHaloBins)
     enddo
     close(1)   

     !----------------------------------------------
     ! output the data file

     open(1,file=outfile,status='unknown',form='formatted')
     write(1,*) numprocs,nbins,nHaloBins
     do i = 1, nbins
        write(1,11) Rad(i), Rad1(i), Rad2(i), corrMMBar(i), CorrMMSig(i),  & 
           CorrHMBar(i,1:nHaloBins),CorrHMSig(i,1:nHaloBins), CorrHHBar(i,1:nHaloBins),CorrHHSig(i,1:nHaloBins)
     enddo
     close(1)   

  endif

! some format statements

  10 format(8(f14.7,2x))
  11 format(30(f14.7,2x))

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  call MPI_BARRIER(MPI_COMM_WORLD,ierr)

! shutdown MPI

  call MPI_FINALIZE(ierr)

end program TwoPointCorrMPI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
